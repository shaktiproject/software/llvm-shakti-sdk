About
=====

 This repository contains instructions for using LLVM support on shakti-sdk.
The linking part is done with gcc, as the lld of llvm for RISC-V is not stable. 


 Download llvm toolchain
======================

```
     git clone https://github.com/llvm/llvm-project.git
```


Building llvm/clang/lld
=======================

Assuming $RISCV is the path where riscv tools are installed (bin folder).
If riscv toolchain is not installed, please click [here](https://github.com/riscv/riscv-tools) 

```
    cd llvm-project
    mkdir build 
    cd build
    cmake -G Ninja -DCMAKE_BUILD_TYPE="Release"   -DBUILD_SHARED_LIBS=True \ 
       -DLLVM_USE_SPLIT_DWARF=True   -DLLVM_ENABLE_PROJECTS="clang;clang-tools-extra;lld" \
       -DCMAKE_INSTALL_PREFIX="$RISCV/riscv64" -DLLVM_OPTIMIZED_TABLEGEN=True \
       -DLLVM_BUILD_TESTS=False   -DDEFAULT_SYSROOT="$RISCV/riscv64/riscv64-unknown-elf" \
       -DLLVM_DEFAULT_TARGET_TRIPLE="riscv64-unknown-elf" \
       -DLLVM_TARGETS_TO_BUILD="RISCV"   ../llvm

    cmake --build . --target install 
```

Setting up the tool chain
=========================

```
  export PATH=$PATH:$RISCV/bin:$RISCV/lib
  export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$RISCV/lib
  export PATH=$PATH:$RISCV/riscv64-unknown-elf/bin
  export PATH=$PATH:$RISCV/riscv64-unknown-elf/lib
 ```
   
note: We need to export PATH variables accordingly so that clang could access the headers and libraries required for riscv architecture

Test Hello world in Spike
===========================

    #include<stdio.h>
    int main() 
    {
      printf("hello world");
      return 0;
    }
 
 ```
clang -O -c hello.c --target=riscv64
riscv64-unknown-elf-gcc hello.o -o hello -march=rv64imac -mabi=lp64

spike $(which pk) hello
bbl loader
hello world
```

Downloading llvm-shakti-sdk
==========================

``` 
git clone https://gitlab.com/shaktiproject/software/llvm-shakti-sdk
cd llvm-shakti-sdk
```

You have llvm supported shakti sdk ready.

Start Coding !


Build
=====

     #To build all programs for default target
       make all
     #To build specific programs example:Hello
       make all PROGRAM=hello
     #we can specify target board for building by using 
       make all PROGRAM=hello TARGET=artix_100t #likewise for other targets
   
     #The executables and objdump files generated during make can be found under output folder under each applications folder.

Please refer https://shakti.org.in/shaktisdk.html for more details on shakti-sdk.
